Conda Environment for Nanopore Amplicon Dereplication Workflow
==============================================================

Version: 0.1

This is the Makefile and environment.yml that can be used to build a Conda environment for the Nanopore Amplicon Dereplication GeneFlow workflow. The workflow can be found here: https://gitlab.com/geneflow/workflows/nanopore-amplicon-dereplication-gf.git. 

Requirements
------------

Conda (Anaconda or Miniconda) must be installed before building this Conda environment. You can install Miniconda by running the packaged script (install_miniconda.sh), or using the instructions here: https://docs.conda.io/en/latest/miniconda.html. 

Instructions
------------

First, clone the conda environment repository and enter the directory:

.. code-block:: bash

    git clone https://gitlab.com/geneflow/conda/nanopore-amplicon-dereplication.git
    cd nanopore-amplicon-dereplication

If you don't already have Conda installed, you can install it by running the install script:

.. code-block:: bash

    make install-miniconda
    source ~/.bashrc

To build the environment, run the following:

.. code-block:: bash

    make env-file

This should create a Conda environment located in the "env" directory. Activate with the following:

.. code-block:: bash

    conda activate ./env
